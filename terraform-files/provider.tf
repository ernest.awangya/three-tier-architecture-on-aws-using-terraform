terraform {
  required_version = "~> 1.5.4"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
  #     backend "s3" {
  #     bucket = "three-tier-architecture-bucket"
  #     key    = "terraform.tfstate"
  #     region = "us-east-1"
  #   }
}

provider "aws" {
  region = var.region-name
}