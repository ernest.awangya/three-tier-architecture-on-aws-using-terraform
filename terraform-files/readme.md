Target Group:
instance type
- instances

Target Group Name:
protocol/port: tttp/80
protocol version: http1
Health check protocol: http
Health check path: /

Advanced health check settings
- port:Traffic port
- health threshold: 5
- unhealthy threshold: 2
- timeout: 5
- interval: 30secs
- success code: 200-399

Tags: key/value

Register instances: 2/2, port for selected instances: 80

CREAT TARGET

CREATE ALB
Name:
Scheme: internet facing
ip address type: ipv4

Listeners
loadbalancer protocol/ LB port: http/80
AZs:
- VPC
- AZs

Security Groups