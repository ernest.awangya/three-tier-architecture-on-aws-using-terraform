# Multi-Tier Architecture Overview
I've implemented a three-tier architecture that comprises a presentation layer (Web tier), an application layer (App tier), and a database layer (Database tier). I've ensured high scalability and availability by employing an Auto-Scaling Group and an Elastic Load Balancer (ALB) across multiple Availability Zones (AZs). These three layers collaboratively function to deliver the desired final product. 

 __Web Tier:__ This tier handles incoming user requests and can be horizontally scaled for increased capacity. It typically includes web servers and a load balancer for distributing traffic.

__Application Tier:__ Application servers run our business logic and interact with the database tier. They can also be horizontally scaled to meet demand.

__Database Tier:__ The database stores and manages our application data. In this architecture, we use Amazon RDS for a managed database service.

# Architecture Diagram

![alt text](https://miro.medium.com/v2/resize:fit:720/format:webp/0*AOuJQtwUjJlpWHyw.png)

# Getting Started
## Prerequisites
Before you get started, make sure you have the following prerequisites in place:

- Terraform installed.
- AWS IAM credentials configured.
- SSH keys (If you don't have them yet)

## Features
__High Availability:__ The architecture is designed for fault tolerance and redundancy.

__Scalability:__ Easily scale the web and application tiers to handle varying workloads.

__Security:__ Security groups and network ACLs are configured to ensure a secure environment.

## Terraform Configuration
The Terraform configuration for this project is organized into different sections and resources to create the necessary AWS infrastructure components. Key resources include:

- Virtual Private Cloud (VPC)
- Subnets and Route Tables
- Security Groups and Network ACLs
- Load Balancers
- Auto Scaling Groups
- RDS Database Instances

## Deploying the Architecture
Open the __terminal__
> **Warning:** Before executing terraform commands, ensure that you have generated your SSH keys. If you haven't done so yet, create them by running the command below, else your terraform apply will fail. Alternative, manually create a keypair on the AWS console and name it ```devops-key```
 
```
ssh-keygen
```

Go to the folder where the terraform configurations files are saved

Initialize the __terraform directory__ by running this below command:
```
 terraform init
```

Check the syntax and validate the configuration files by running this below command:
```
terraform validate 
```

See what are the resources are going to be created using the below command:
``` 
terraform plan -var-file=secret.tfvars 
```

Run the terraform apply command for Creating Resources in AWS
``` 
terraform apply -var-file=secret.tfvars
```

## Resource Creation Verification

Now , You can Log into your AWS account and check whether our resources are created or not.

## Multi Tier Architecture Verification

You can Verify our work by the below methods,

- Paste the Output DNS into your browser and check whether our Web application is running or not.
- Try to log into Application Servers through our Web Servers using SSH
- Try to log into MySQL Server from Application Server.
By this methods you can verify the Architecture work.

## Delete the stack

To delete the resources created, run the command:
``` 
terraform destroy -var-file=secret.tfvars 
```